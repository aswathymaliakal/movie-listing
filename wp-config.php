<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'movies' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'B$V7#KK&NV.7+g3+<$1Kx=9No`|qXN#FU/@Rg#6d2CsjwG4s6I:/r%+,25`~ZpT*' );
define( 'SECURE_AUTH_KEY',  'CSXz,mj2FevJOk/A~=M/~]g+~#EfQbpB/[0Ie~y2#jhK! MtS</FvZrab];A(Itd' );
define( 'LOGGED_IN_KEY',    'GPg+{)nSF ,~2C?YTQ(fHzXbpJ-7(wqj4G%8g)R<<^>>p,>*D>x])#WSw}Zk.pAD' );
define( 'NONCE_KEY',        '_aO}Ybp~fb[eogbg@.HVG`tT@^hHK+?#x*2RFRbO!&KYcbJZxyMSO=X]>(2gtUp}' );
define( 'AUTH_SALT',        '$]=d}n7;C-FzXbQ1V|<dKs[D!?%;0@<w=.Vs1r-lfl g@Fn`Q=962W^MT5S|0EZr' );
define( 'SECURE_AUTH_SALT', '_r_{4K6S;*G4ZY$Ayg2=lydVqOqq6*k<y$ZePiPB-SP})eDm: (awM96<jKwlXwQ' );
define( 'LOGGED_IN_SALT',   'j|eN3avs|=^db5*aImCZJ.^ld0J)`)DEhZb>(>^L>QbxOKRd-4Mt943}/dA`{!8a' );
define( 'NONCE_SALT',       'o+ZsQB9#Ceo{u1HRq3|j$/V2%:ls,bcgGX|C)Z6IT`ao*r?ELa]85O4X7WSb~=!y' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
