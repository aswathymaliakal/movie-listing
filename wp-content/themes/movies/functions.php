<?php
/**
 * Fileupload functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Fileupload
 * @since Fileupload 1.0
 */
add_theme_support( 'post-thumbnails', array( 'post', 'movies' ) );
// custom post type function
function create_posttype() {
  
    register_post_type( 'movies',
          array(
            'labels' => array(
                'name' => __( 'Movies' ),
                'singular_name' => __( 'Movie' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'movies'),
            'show_in_rest' => true,
			'menu_icon'  => 'dashicons-video-alt2',
            'taxonomies' => array( 'category' ),
			'supports' => array( 'title', 'editor', 'author', 'thumbnail')
        )
    );
}
add_action( 'init', 'create_posttype' );

//* Metabox for Video Post Type*//

function add_custom_meta_box()
{
    add_meta_box("movie-meta-box", "Movie Details", "custom_meta_box_markup", "movies", "side", "high", null);
}

add_action("add_meta_boxes", "add_custom_meta_box");

function custom_meta_box_markup($object)
{
    wp_nonce_field(basename(__FILE__), "meta-box-nonce");

    ?>
        <div class="components-base-control">
            <label for="duration">Enter Duration in Minute</label><br>
            <input name="duration"  class="components-text-control__input" type="number" value="<?php echo get_post_meta($object->ID, "duration", true); ?>">
            <br>
            <label for="link">Video Link</label><br>
            <input name="link" type="text" class="components-text-control__input" value="<?php echo get_post_meta($object->ID, "link", true); ?>">
            <br>
            <label for="director">Enter Director Name</label><br>
            <input name="director" type="text" class="components-text-control__input" value="<?php echo get_post_meta($object->ID, "director", true); ?>">
            <br>
            <label for="release">Select Release Date</label><br>
            <input name="release" type="date" class="components-text-control__input" value="<?php echo get_post_meta($object->ID, "release", true); ?>">
            <br>
            <label for="status">Current Status</label><br>
            <select name="status" class="components-text-control__input" style="padding: 0px 0px ;">
                <?php 
                    $option_values = array('Announced','Pre Production', 'Production', 'Post Production', 'Released');

                    foreach($option_values as $key => $value) 
                    {
                        if($value == get_post_meta($object->ID, "status", true))
                        {
                            ?>
                                <option selected><?php echo $value; ?></option>
                            <?php    
                        }
                        else
                        {
                            ?>
                                <option><?php echo $value; ?></option>
                            <?php
                        }
                    }
                ?>
            </select>

            <br>

           
        </div>
    <?php  
}


//* Save Metabox Datas *//
function save_custom_meta_box($post_id, $post, $update)
{
    if (!isset($_POST["meta-box-nonce"]) || !wp_verify_nonce($_POST["meta-box-nonce"], basename(__FILE__)))
        return $post_id;

    if(!current_user_can("edit_post", $post_id))
        return $post_id;

    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;

    $slug = "movies";
    if($slug != $post->post_type)
        return $post_id;

    $duration_value = "";
    $link_value = "";
    $director_value = "";
    $release_value = "";
    $status_value = "";
 
    if(isset($_POST["duration"]))
    {
        $duration_value = $_POST["duration"];
    }   
    update_post_meta($post_id, "duration", $duration_value);
    if(isset($_POST["link"]))
    {
        $link_value = $_POST["link"];
    } 
    update_post_meta($post_id, "link", $link_value);

    if(isset($_POST["director"]))
    {
        $director_value = $_POST["director"];
    } 
    update_post_meta($post_id, "director", $director_value);

    if(isset($_POST["release"]))
    {
        $release_value = $_POST["release"];
    }   
    update_post_meta($post_id, "release", $release_value);

    if(isset($_POST["status"]))
    {
        $status_value = $_POST["status"];
    }   
    update_post_meta($post_id, "status", $status_value);


}

add_action("save_post_movies", "save_custom_meta_box", 10, 3);


function wpd_movies_query( $query ){
    if( ! is_admin()
        && $query->is_post_type_archive( 'movies' )
        && $query->is_main_query() ){
            $query->set( 'posts_per_page', 6 );
    }
}
add_action( 'pre_get_posts', 'wpd_movies_query' );


// Ajax More Loading
wp_localize_script( 'twentyfifteen-script', 'ajax_posts', array(
    'ajaxurl' => admin_url( 'admin-ajax.php' ),
    'noposts' => __('No older posts found', 'twentyfifteen'),
));

function more_post_ajax(){

    $ppp = (isset($_POST["ppp"])) ? $_POST["ppp"] : 6;
    $page = (isset($_POST['pageNumber'])) ? $_POST['pageNumber'] : 0;

    header("Content-Type: text/html");

    $args = array(
        'suppress_filters' => true,
        'post_type' => 'movies',
        'posts_per_page' => $ppp,
        'paged'    => $page,
    );

    $loop = new WP_Query($args);

    $out = '';

    if ($loop -> have_posts()) :  while ($loop -> have_posts()) : $loop -> the_post();
    $duration = get_post_meta($loop->post->ID,'duration', true);
    $link = get_post_meta($loop->post->ID,'link', true);
    $director = get_post_meta($loop->post->ID,'director', true);
    $status = get_post_meta($loop->post->ID,'status', true);
    $categories = get_the_category(); 
    $cat_name = $categories[0]->cat_name;
    $featured_image = get_the_post_thumbnail();
        $out .= '<div class="col-md-4">
                <div class="card-content">
                    <div class="card-img card-size">'.$featured_image.'
						<span><h4>Status :'.$status.'</h4></span>
                    </div>
                    <div class="card-desc">
                        <h3>'. get_the_title().'</h3>
						<div class="text">  Duration : '.$duration.' Minutes</div>
						<div class="text"> Director : '.$director.'</div>
                        <div class="text"> Category : '.$cat_name.'</div>
                        <p class="desc">'.get_the_excerpt().'></p>
                            <a href="'.get_the_permalink().'" class="btn-card">Read</a>   
                    </div>
                </div>
            </div>';

    endwhile;
    endif;
    wp_reset_postdata();
    die($out);
}

add_action('wp_ajax_nopriv_more_post_ajax', 'more_post_ajax');
add_action('wp_ajax_more_post_ajax', 'more_post_ajax');