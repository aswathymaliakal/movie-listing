<?php
get_header();
?>

<!-- details card section starts from here -->
<section class="details-card">
    <div class="container">
        <div class="row">
		<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : ?>
		<?php the_post();
	    global $post;
		$duration = get_post_meta($post->ID,'duration', true);
		$link = get_post_meta($post->ID,'link', true);
		$director = get_post_meta($post->ID,'director', true);
		$status = get_post_meta($post->ID,'status', true);
        $categories = get_the_category(); 
        $cat_name = $categories[0]->cat_name;
        $featured_image = get_the_post_thumbnail();
        $release = get_post_meta($post->ID,'release', true);
        $release = date('j-n-Y', strtotime($release));
   		?>
            <div class="col-md-4">
                <div class="card-content">
                    <div class="card-img card-size">
                        <?php 
                        if($featured_image)
                        {
                        echo $featured_image;
                        } else
                        { ?>
                         <img src="https://placeimg.com/380/230/nature" alt="">
                         <?php } ?>
                         <span class="card-img-animation"><h4>Status : <?php echo $status; ?> </h4></span>
                    </div>
                    <div class="card-desc">
                        <h3><?php the_title(); ?></h3>
						<div class="text">  Duration : <?php echo $duration; ?> Minutes</div>
						<div class="text"> Director : <?php echo $director; ?></div>
                        <div class="text"> Category : <?php echo $cat_name; ?></div>
                        <div class="text"> Release : <?php echo $release; ?></div>
                        <p class="desc"><?php echo get_the_excerpt(); ?></p>
                            <a href="<?php echo get_the_permalink(); ?>" class="btn-card">Read</a>   
                    </div>
                </div>
            </div>
			<?php endwhile; ?>
            <div id="load_more_posts"></div>
        </div>
    </div>
</section>
<!-- details card section starts from here -->


	<!-- <div id="more_posts">Load More</div> -->

<?php else : ?>
	<?php echo 'No Blog here...'; ?>
<?php endif; ?>

<?php get_footer('list'); ?>

