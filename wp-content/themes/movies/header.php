<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Movies </title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/>  
    <link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/assets/css/custom.css"/>  
    <?php  wp_head(); ?>
  </head>
  <header><div class="container">
	<div class="row main-grid">
	    <div class="main-heading">
     
	        <h1 class="heading" > <a href="<?php echo site_url(); ?>">Video Listing</a></h1>
	    </div>
	</div>
</div></header>
  <body <?php body_class(); ?>>
