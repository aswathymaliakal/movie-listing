<?php
get_header();
?>

<?php if ( have_posts() ) : ?>

<?php while ( have_posts() ) : ?>
	<?php the_post(); 
    global $post;
    $duration = get_post_meta($post->ID,'duration', true);
    $link = get_post_meta($post->ID,'link', true);
    $director = get_post_meta($post->ID,'director', true);
    $status = get_post_meta($post->ID,'status', true);
    $release = get_post_meta($post->ID,'release', true);
    $categories = get_the_category(); 
    $cat_name = $categories[0]->cat_name;
    $featured_image = get_the_post_thumbnail();
    ?>
<!-- details card section starts from here -->
<section class="details-card">
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card-content">
                    <div class="card-img" >
                       <a href="#myModal" data-toggle="modal">                        <?php 
                        if($featured_image)
                        {
                        echo $featured_image;
                        } else
                        { ?>
                         <img src="https://placeimg.com/380/230/nature" alt="">
                         <?php } ?> </a>
                    </div>
                    <div class="card-desc">
                        <h3><?php the_title(); ?></h3>
						<div class="text">  Duration : <?php echo $duration; ?> Minutes</div>
						<div class="text"> Director : <?php echo $director; ?></div>
                        <div class="text"> Category : <?php echo $cat_name; ?></div>
                        <div class="text"> Release : <?php echo $release; ?></div>
                        <div class="text"> Status : <?php echo $status; ?></div>
                        
                        <p><?php the_content(); ?></p>

                        <!-- Button for Popup -->
                         <a href="#myModal" class="btn btn-primary btn-lg" data-toggle="modal">Play Video</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- details card section starts from here -->
    
    <!-- Modal HTML -->
    <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title"> <?php the_title(); ?> </h3>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                  <div class="embed-responsive embed-responsive-16by9">
                    <iframe id="cartoonVideo" class="embed-responsive-item" width="560" height="315" src="<?php echo $link; ?>" allowfullscreen></iframe>
                  </div>
                </div>
            </div>
        </div>
    </div>



	<?php endwhile; ?>

<?php else : ?>
	<?php echo 'No Blog here...'; ?>
<?php endif; ?>

<?php get_footer(); ?>
